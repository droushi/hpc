#ifndef DEF_H
#define DEF_H


// I used an artificially augmented dataset to be able to benchmark parallel performance.
// At a dataset size of 1000 and trivial processing workload, serial implementation is much more performant.
//#define DATASET_SIZE 31334400
#define DATASET_SIZE 1000

//Uncomment to benchmark the parallel performance
//#define MP_PROFILE


//Uncomment to enable debug logs, they aren't very useful right now but it is a good idea to keep
//debug scaffoldoing in place
//#define DEBUG

#ifdef DEBUG
	#define DEBUG_OUT(x) std::cout x
#else
	#define DEBUG_OUT(x)
#endif

//The type representation of numbers used across all functions
typedef short NumberType;

#endif