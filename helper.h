#ifndef HELPER_H
#define HELPER_H

#include "def.h"
#include <memory>
#include <string>
#include <fstream>
#include <vector>

template <typename T>
struct Result {
	long long product_value = -1;	//The value of the product
	int product_offset = -1;		//The offset of the first number in the product
	int seq_len = -1;				//The number of elements in the sequence that produced this product
	std::vector<T> sequence;		//The sequence of numbers

	Result(int len) {
		seq_len = len;
	}
};

/// \details A function that reads a file and returns an array of numbers, templated over element count and element data type. 
/// I opted for static buffer size at compile time because the problem statement doesn't specify a change.
/// If the number of elements was changing I would allow change at runtime by not fixing the input size
/// \tparam T is data type used to represent the elements
/// \tparam BufferSize is the fixed buffer size used, for this assignment it is fixed to 1000
/// \param[in]  filename is the path to the file that we will read the numbers from
/// \return a smart pointer to an array of numbers of type T
template <typename T, unsigned int BufferSize>
std::shared_ptr<T> read_elements(const std::string filename)
{

	std::ifstream ifs(filename, std::ios::binary);
	if (!ifs)
	{
		return nullptr;
	}

	//Create a smart pointer and allocate an array, we also make sure that deletion will deallocate all elements
	std::shared_ptr<T> elements(new T[BufferSize], std::default_delete<T[]>());

	char c;
	long counter = 0;
	while (counter < BufferSize && ifs.get(c))
	{
		//Only read digits, obviosuly this ignores valid numbers such as -1 
		//but let's not over complicate things.
		if (c < 48 || c > 57)
		{
			continue;
		}
		elements.get()[counter] = (T)atoi(&c);
		if (counter % 50 == 0)
		{
			DEBUG_OUT(<< std::endl);
		}
		DEBUG_OUT(<< elements.get()[counter]);
		counter++;
	}

	return elements;

}

#endif