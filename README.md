# HPC Problem solution


## Build instructions
You should be able to build this code using any C++ compiler than supports C++14 standard and OpenMP. Below is how to build this code using Cmake.

1. ```git clone https://droushi@bitbucket.org/droushi/hpc.git```
2. ```git checkout serial_solution``` or ```git checkout paralell_solution```
2. Make a build directory under the repo root
```mkdir build```
3. ```cd build```
4. ```cmake ..```
5. The project files will be generated. The next step depends on the build environment you use
  e.g. for make builds you would run ``make`` for Visual Studio you open ```HPC_Problem.sln```
  

## Run instructions
The executable takes one mandatory command line parameter which is the path to the dataset text file that contains the numbers.

The serial application outputs the 8-digit long sequence with the highest product. The number 8 is hardcoded but can be made to come from command line trivially.

###### Sample output
```Largest Product of 8 element long sequence = 9 x 8 x 7 x 9 x 7 x 9 x 5 x 9 = 12859560```


The parallel application outputs the 4-digit long and 8-digit long sequences with the highest product. The numbers 4 and 8 are hardcoded but can be made to come from command line trivially. The parallel version uses OpenMP, the number of threads used isn't specified by the code and should come from the environment variables.
###### Sample output
```Largest Product of 4 element long sequence = 8 x 9 x 9 x 9 = 5832```

```Largest Product of 8 element long sequence = 9 x 8 x 7 x 9 x 7 x 9 x 5 x 9 = 12859560```



## Files
| File          | Function      											  |
| ------------- |:-----------------------------------------------------------:| 
| main.cxx      | entry point, parsing command line and drive the application | 
| solution.h    | contains the function that find the request sequence  	  | 
| helper.h      | contains the function that parses input file      		  | 
| def.h         | contains some definitions and structs      				  | 