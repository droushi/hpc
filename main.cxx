#include "def.h"		// Definitions and some constants
#include "helper.h"		// Input file parsing
#include "solution.h"	// The actual code for the solution

#include <iostream>
#include <memory>

int main(int argc, char *argv[]) {
	
	if (argc < 2) {
		std::cerr << "Please specify dataset file" << std::endl;
		return 1;
	}

	std::shared_ptr<NumberType> numbers = read_elements<NumberType, DATASET_SIZE>(argv[1]);
	if (!numbers) {
		std::cerr << "Failed to read numbers from file" << std::endl;
		return 1;
	}

	
	int seq_len = 8;	//the length of the sequences
	std::shared_ptr<Result<NumberType>> pRes = find_largest_product<NumberType, DATASET_SIZE>(numbers, seq_len);
	if (!pRes) {
		std::cerr << "Failed to find largest product, sequence length maybe invalid" << std::endl;
		return 1;
	}


	//Spell out the numbers that make up the largest product 
	std::cout << "Largest Product of " << pRes->seq_len << " element long sequence = ";
	for (auto it_seq = pRes->sequence.begin(); it_seq != pRes->sequence.end(); ++it_seq) {
		if (it_seq != pRes->sequence.begin()) {
			std::cout << " x ";
		}
		std::cout << *it_seq;
	}
	//Finally output the product
	std::cout << " = " << pRes->product_value << std::endl;

	int x;
	std::cin >> x;
	
	return 0;
}

