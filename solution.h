#ifndef SOLUTION_H
#define SOLUTION_H
#include "def.h"
#include <memory>

/// \details A serial function that finds the largest product of contiguous elements in a list of numbers. It is
/// templated over element count and element data type. I opted for static buffer size at compile time because the 
/// the problem statement doesn't specify a change.
/// \tparam T is data type used to represent the numbers
/// \tparam BufferSize is the fixed buffer size used, for this assignment it is fixed to 1000
/// \param[in]  numbers is a smart pointer to an array of numbers to operate on
/// \param[in]  seq_len is one of the length of the sequence length whose greatest product is to be found
/// \return a pointer to a result object
template <typename T, unsigned int BufferSize>
std::shared_ptr<Result<T>> find_largest_product(const std::shared_ptr<T> numbers, const short seq_len) {

	if (seq_len < 1 || seq_len > BufferSize) {
		//Invalid seq_len, return null
		return nullptr;
	}

	std::shared_ptr<Result<T>> final_result = std::make_shared<Result<T>>(seq_len);

	T* pNumbers = numbers.get();
	for (unsigned int i = 0; i < BufferSize - seq_len + 1; ++i) {
		long long product = 1;
		for (unsigned int j = i; j < i + seq_len; ++j) {
			product *= pNumbers[j];
			DEBUG_OUT(<< pNumbers[j] << " ");
		}
		DEBUG_OUT(<< " = " << product << std::endl);
		if (product > final_result->product_value) {
			final_result->product_value = product;
			final_result->product_offset = i;
		}
	}

	for (int j = final_result->product_offset; j < final_result->product_offset + final_result->seq_len; ++j) {
		final_result->sequence.push_back(pNumbers[j]);
	}
	
	return final_result;
}
#endif